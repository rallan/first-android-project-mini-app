package com.example.miniapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, android.widget.CompoundButton.OnCheckedChangeListener {
    private Button button;
    private ListView itemsList;
    private ArrayList<String> items;
    private ArrayAdapter<String> adapter;
    public static String data = "";
    public static int pos = 0;
    public static String NewData = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.AddItemActivityButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPause();

            }
        });

        itemsList = findViewById(R.id.items_list);
        items = FileHelper.readData(this);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, items);
        itemsList.setAdapter(adapter);
        button.setOnClickListener(this);
        adapter.notifyDataSetChanged();
        itemsList.setOnItemClickListener(this);
        itemsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                items.remove(position);
                adapter.notifyDataSetChanged();
                DeleteFromFile();
                return false;
            }
        });
    }

    public void openAddItem() {
        Intent intent = new Intent(this, addItem.class);
        startActivity(intent);
        onStop();
    }
    public void openModifyItem() {
        Intent intent = new Intent(this, ModifyItem.class);
        startActivity(intent);
        onStop();
    }

    @Override
    public void onClick(View v) {
        openAddItem();
    }

    @Override
    protected void onResume () {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public void DeleteFromFile() {

        FileHelper.writeDate(items, this);
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        pos = position;
        data = items.get(position);
        openModifyItem();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int pos = itemsList.getPositionForView(buttonView);
        String value = items.get(pos);
        items.remove(pos);
        adapter.add(value);
        FileHelper.writeDate(items, this);
        adapter.notifyDataSetChanged();
    }



        public static int GetPos(){
        return pos;

        }
}






