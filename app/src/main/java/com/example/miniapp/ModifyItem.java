package com.example.miniapp;
        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ListView;
        import android.widget.Toast;
        import java.util.ArrayList;

public class ModifyItem extends AppCompatActivity implements View.OnClickListener{

    private EditText itemEdit;
    private Button Button;

    public ArrayList<String> itemsArray;
    public ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        itemEdit = findViewById(R.id.item_edit_text);
        Button = findViewById(R.id.CreateItemButton);
        itemsArray = FileHelper.readData(this);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemsArray);
        Button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.CreateItemButton: String itemEntered = itemEdit.getText().toString();

            int position = MainActivity.GetPos();
            itemsArray.set(position, itemEntered);

                itemEdit.setText("");
                FileHelper.writeDate(itemsArray, this);
                Toast.makeText(this, "Item Added", Toast.LENGTH_SHORT).show();

                break;
        }

        finish();
        openMainActivity();
    }
    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        onStop();
    }

}