Team Member list - Ricky Allan
Device Name - Testing using a Nexus 2 device
Project/App Title - Mini App
Basic instructions on usage - 
    the Add item button will open a second activity to add an item to the list
    Clicking on an item on the list will open the Modify Item Activity
    Long clicking on an item will delete the item
    
Any special info we need to run the app- none

Lessons Learned - Learning the syntax and required structure of the application building process was a bit harder than I had anticipated.
                I think that I have been able to figure out most of the main concepts, but there are still a few smaller ones that elude me.
